const mongoose = require("mongoose");
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.z2lte8d.mongodb.net/b203_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("Connected to the cloud database"));
const taskSchema = new mongoose.Schema(
		{
			name: {
				type: String,
				required: [true, "Task name is required"]
			},
			status: {
				type: String,
				default: "pending"
			}
		}
);
const Task = mongoose.model("tasks", taskSchema);

const express = require("express");
const port = 3001;
const app = express();
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Business Logic
/*
    1. Add a functionality to check if there are duplicate tasks
        - If the task already exists in the database, we return an error message "Duplicate task found"
        - If the task doesn't exist in the database, we add it in the database
        A task doesn't exists if:
            - result from the query is not null
            - result.name is equal to req.body.name
    2. The task data will be coming from the request's body
    3. Create a new Task object with a "name" field/property
    4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) => {
		if(result != null && result.name == req.body.name){
                return res.send("Duplicate task found!");        
        }else {
              	// "newTask" was created/instantiaited from the Mongoose schema and will gain access to ".save" method
                let newTask = new Task({
                    name: req.body.name
                });

                newTask.save((saveErr, savedTask) => {
                    if(saveErr){
                        return console.error(saveErr)
                    }
                    else{
                        return res.status(201).send("New Task created!");
                    }
                })
            }
	});
});

app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err);
		}else{
			return res.status(200).send({
				data: result
			});
		}
	});
});


const usersSchema = new mongoose.Schema(
		{
			username: {
				type: String,
				required: [true, "Username is required"]
			},
			password: {
				type: String,
				required: [true, "Password is required"]
			}
		}
);
const User = mongoose.model("users", usersSchema);

/*
	2. Create a POST route that will access the "/signup" route that will create a user.
	5. Process a POST request at the "/signup" route using postman to register a user.
*/

app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if(err){
			return res.send(err);
		}else{
			if(result != null && result.name == req.body.name){
				res.send("Username is taken");
			}else{
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				});

				newUser.save((saveErr, savedUser) => {
					if(saveErr){
						return res.send(saveErr);
					}else{
						return res.status(200).send("User created successfully.");
					}
				});
			}
		}
	});
});

app.listen(port, () => console.log(`Server is running at localhost:${port}`));